<?php
/**
 * @file
 * issue_view.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function issue_view_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|issue|default';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'issue';
  $ds_fieldsetting->view_mode = 'default';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
      'formatter_settings' => array(
        'link' => '0',
        'wrapper' => 'h2',
        'class' => '',
      ),
    ),
    'submitted_by' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'ds_post_date_short',
    ),
  );
  $export['node|issue|default'] = $ds_fieldsetting;

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|issue|kanban_board';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'issue';
  $ds_fieldsetting->view_mode = 'kanban_board';
  $ds_fieldsetting->settings = array(
    'title' => array(
      'weight' => '0',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'submitted_by' => array(
      'weight' => '4',
      'label' => 'hidden',
      'format' => 'ds_post_date_short',
    ),
  );
  $export['node|issue|kanban_board'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function issue_view_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|issue|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'issue';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'ds_3col_equal_width';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'title',
        1 => 'field_status',
        2 => 'field_project',
        3 => 'field_user',
        4 => 'submitted_by',
      ),
    ),
    'fields' => array(
      'title' => 'left',
      'field_status' => 'left',
      'field_project' => 'left',
      'field_user' => 'left',
      'submitted_by' => 'left',
    ),
    'classes' => array(),
    'wrappers' => array(
      'left' => 'div',
      'middle' => 'div',
      'right' => 'div',
    ),
    'layout_wrapper' => 'section',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => 1,
  );
  $export['node|issue|default'] = $ds_layout;

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|issue|kanban_board';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'issue';
  $ds_layout->view_mode = 'kanban_board';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'regions' => array(
      'ds_content' => array(
        0 => 'title',
        1 => 'field_status',
        2 => 'field_project',
        3 => 'field_user',
        4 => 'submitted_by',
      ),
    ),
    'fields' => array(
      'title' => 'ds_content',
      'field_status' => 'ds_content',
      'field_project' => 'ds_content',
      'field_user' => 'ds_content',
      'submitted_by' => 'ds_content',
    ),
    'classes' => array(),
    'wrappers' => array(
      'ds_content' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
    'layout_disable_css' => FALSE,
  );
  $export['node|issue|kanban_board'] = $ds_layout;

  return $export;
}

/**
 * Implements hook_ds_view_modes_info().
 */
function issue_view_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'kanban_board';
  $ds_view_mode->label = 'Kanban_board';
  $ds_view_mode->entities = array(
    'node' => 'node',
    'user' => 'user',
  );
  $export['kanban_board'] = $ds_view_mode;

  return $export;
}

For using this module, you need to make some pre installations:
- Create Node type Project (machine_name = 'project')
	- Only needs a title
- Create Node type Issue (machine name = 'issue')
	- Title
	- Description
	- Status (On hold, open, in progress, done)
	- Project (reference to Project)
	- User (reference to user)

You can do it with the help of Features, install module is in issue_view-7.x-1.1.tar
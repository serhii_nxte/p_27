<?php
/**
 * Created by PhpStorm.
 * User: serhii
 * Date: 25.06.18
 * Time: 18:41
 */

// connecting 'p_27.module'
require_once dirname(__FILE__).'/p_27.module';

/**
 * @param $node
 *             function to create board filtered by project
 */
function p_27_board_node_filtered($node) {
  if ($node->type != 'project') {
    drupal_set_message('Impossible to filter issue with non-project node', 'error');
    drupal_goto(current_path().'/..', array(), 301);
  }
  // getting status and weight
  $table_27 = p_27_get_all_issue_to_project($node->nid);

  $header = [ array('data' => t('Created')),
    array('data' => t('In progress')),
    array('data' => t('Done')),
    array('data' => t('Deleted'))];
  $rows = array();

  // status names, give name to every status
  $status_names = array('created', 'progressed', 'done', 'deleted');

  foreach($table_27 as $nid => $element) {

    foreach ($element as $weight => $status) {
      $node = node_load($nid);
      $node_view = node_view($node);

      $links_view = _p_27_get_links_view($status, $nid, isset($node_view['field_user']) ?
        $node_view['field_user']['#items'][0]['entity']->uid : -1);

      $row[$status] = [
        'data' => array($node_view, $links_view),
        'id' => $status_names[$status].'-' . $node->vid,
      ];

      // setting empty elements on the table
      for($i = 0; $i < 4; $i++){
        if ($status != $i) {
          $row[$i] = [
            'data' => array(),
            'id' => $status_names[$i].'-' . $node->vid,
          ];
        }
      }
      ksort($row);
      $rows[] = $row;
    }

  }
  ksort($rows);
  // filter project form
  $content = drupal_get_form('p_27_project_filter_form');

  $content['raw_table'] = array(
    '#theme' => 'table',
    '#header' =>$header,
    '#rows' => $rows,
    '#empty' => t('No tasks found. Please create one to start working on it.'),
    '#attributes' => array(
      'id' => 'kanban-table',
    ),
  );
  return $content;
}

function p_27_get_all_issue_to_project($pid) {
  $iid_array = array();
  $result = db_select('table_27', 't')
    ->fields('t')
    ->execute()->fetchAll();
  foreach ($result as $value) {
    $issue = node_load($value->nid);
    if (isset($issue->field_project['und']['0'])
      && $issue->field_project['und']['0']['target_id'] == $pid) {
      $iid_array[] =  $issue->nid;
    }
  }
  $table_27 = array();
  // get info from 'table_27'
  foreach ($result  as $issue) {
    if (in_array($issue->nid, $iid_array) ) {
      $table_27[$issue->nid] = [$issue->weight => $issue->status];
    }
  }
  return $table_27;
}